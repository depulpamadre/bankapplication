# BankApplication

Ejercicio final de la TechU

![Final de curso](http://hylandsestate.co.uk/wp-content/uploads/2018/09/monsters_university_2013-2880x1800.jpg)

## Partes

* [Core](https://bitbucket.org/depulpamadre/bankapplication/src/master/Core/Readme.md)

* [Front](https://bitbucket.org/depulpamadre/bankapplication/src/master/Front/README.md)

## Conceptos

![Arquitectura](https://bitbucket.org/depulpamadre/bankapplication/raw/76b6148652f5dfe3b67e89b9c288758c7ec59c43/Front/images/Conceptos.png)
