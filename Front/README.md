# Frontal de la aplicacion

Practica de finalización de la Tech University 2018

## Dependencias

* [Node.js](http://nodejs.org)
* [Bower](http://bower.io) `npm install bower -g`
* [bootstrap](https://getbootstrap.com/)

### Funcionalidades base

> * Alta de usuarios
> * Gestión de Login
> * Consulta de movimientos
>   * (Prerequisito: listado de cuentas por usuario)
> * [x]  Datos de otras API

- **Consulta de movimientos** - La aplicación permitirá, a partir de una cuenta, consultar un listado con los movimientos de la misma.

- **Alta de movimientos** -
A las cuentas se les podrán añadir movimientos. Para la funcionalidad base, vale con que haya dos tipos: ingreso y reintegro.

- **Alta de usuarios** -
Se podrá registrar a un usuario en la aplicación.

- **Gestión de Login** -
Los usuarios dados de alta en la aplicación podrán, utilizando sus credenciales, acceder la aplicación (login). También podrán cerrar sesión con la aplicación (logout).

- **Datos de otras API** -
La aplicación deberá mostrar algún tipo de dato consumido de otra API REST, al margen de las ya propuestas (mLAB, Apitechu). Ejemplos: cambio de divisa, previsión del tiempo, localización geográfica.

- **Funcionalidad Custom** -
Toda aquella funcionalidad significativa añadida a la base, por ejemplo:
· Transferencias/traspasos entre cuentas.
· Modificación de datos del usuario.
· Baja de cuentas.

- **Extras** interesantes (si la funcionalidad base está hecha) -
· Usar JWT.
· Usar Web Components y/o Cells.
· Hacer tests unitarios (Mocha/Chai/Chai-http).

### Realizado

> * [x] Alta de usuarios
> * [x] Gestión de Login
>   * [x] Login usuario
>   * [x] Login administrador
> * [x] Consulta de movimientos
> * [x] Ingresos
> * [x] Depósitos
> * [x] Transferencias
> * [x] Posición
> * [x] (Prerequisito: listado de cuentas por usuario)
> * [x]  Alta de movimientos
>   * [x] Creación de Usuarios
>   * [x] Asignación de cuentas
>   * [x] Hacerse pasar por cliente (*impersonate*)
> * [x]  Datos de otras API
>   * [x]  Frase del día de Chuck Norris XD [chucknorris.io](https://api.chucknorris.io/)
> * [x]  API Rest con 34 Funcionalidades
> * [x]  Pruebas de test de 32 funcionalidades

![Chuck Norris Rules](https://assets.chucknorris.host/img/chucknorris_logo_coloured_small@2x.png)
