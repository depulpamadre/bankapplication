// const io = require ("../io");
const crypt = require("../crypt")
const bcrypt = require("bcrypt")
const requestJson = require('request-json');
const mlabURLBase='https://api.mlab.com/api/1/databases/apitechu8mac/collections/';
require('dotenv').config()
const mlabAPIKey='apiKey=' + process.env.MLAB_API_KEY

//####################################################################
function getAccountsV1 (req,res) {
  console.log("GET /BankPractice/v1/account");
  console.log("getAccountsV1");
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  httpClient.get("account?"+ mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("Devolviendo todas las cuentas");
	           var response = body;
             res.status(200);
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Cuentas no encontradas"
             }
             res.status(404);
          }
      }
      res.send(response);
    }
  );
}
//####################################################################
function getAccountsByIdV1 (req,res) {
  console.log("GET /BankPractice/v1/account/:aid")
  console.log("getAccountsByIdV1");
  var accountId = req.params.aid;
  var query = 'q={"accountId":' + accountId +'}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching account by accountId="+accountId);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("accountId encontrado");
	           var response = body[0];
             res.status(200);
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Cuenta No encontrada"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
function getAccountsByIBANV1 (req,res) {
  console.log("GET /BankPractice/v1/account/IBAN/:IBAN");
  console.log("getAccountsByIBANV1");
  var IBAN = req.params.iban;
  var query = 'q={"IBAN":"' + IBAN +'"}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching account by IBAN="+IBAN);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error accediendo a MLAB"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("IBAN encontrado");
	           var response = body[0];
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "IBAN No encontrado"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
function getAccountsByUserIdV1 (req,res) {
  console.log("GET /BankPractice/v1/account/:userId");
  console.log("getAccountsByUserIdV1");
  var userId = req.params.userId;
  var query = 'q={"userId":' + userId +'}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching account by accountId="+userId);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("Cuentas de usuario encontradas");
	           var response = body;
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Cuenta No encontrada"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
module.exports.getAccountsV1 = getAccountsV1;
//--------------------------------------------------------------------
module.exports.getAccountsByIdV1 = getAccountsByIdV1;
//--------------------------------------------------------------------
module.exports.getAccountsByIBANV1 = getAccountsByIBANV1;
//--------------------------------------------------------------------
module.exports.getAccountsByUserIdV1 = getAccountsByUserIdV1;
//--------------------------------------------------------------------
