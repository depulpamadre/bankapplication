require('dotenv').config()
//const io = require ("../io");
const crypt = require("../crypt")
const bcrypt = require("bcrypt")
const requestJson = require('request-json');
const mlabURLBase='https://api.mlab.com/api/1/databases/apitechu8mac/collections/';
const mlabAPIKey='apiKey=' + process.env.MLAB_API_KEY
//####################################################################
function getUsersV3 (req,res) {
  console.log("GET /BankPractice/v2/users");
  console.log("getUsersV3");
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  var query = 'f={"_id":0}';
  httpClient.get("user?" + query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
       console.log("[Error buscando usuario]");
        var response = {
          "msg" : "Error buscando usuario"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
            console.log("Usuario encontrado");
            res.status(200)
            var response = body;
          } else {
            console.log("[Usuario no encontrado] body.length < 0");
             var response = {
                "msg" : "Usuario no encontrado"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
  );
}
//####################################################################
function getUsersByIdV2 (req,res) {
  console.log("GET /BankPractice/v1/users/:id");
  console.log("getUsersByIdV2");
  var id = req.params.id;
  var query = 'q={"id":' + id +'}&f={"_id":0}';
  console.log("Searching user by id="+id);
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  httpClient.get('user?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error buscando usuario"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("Usuario encontrado");
	           var response = body[0];
             res.status(200)
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Usuarios No encontrado"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
function getUserByNidV1 (req,res) {
  console.log("GET /BankPractice/v1/users/nid/:nid");
  console.log("getUsersByNidV1");
  var id = req.params.nid;
  var query = 'q={"nid":"' + id +'"}&f={"_id":0}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching user by nid="+id);
  var response = {};
  httpClient.get('user?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error buscando usuario"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("Usuario encontrado");
	           var response = body[0];
             res.status(200)
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Usuarios No encontrado"
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
function createUsersV4 (req,res) {
  // falta probar cuando no hay nada
  // falta ver que hacer con los huecos cerrados
      console.log("POST /BankPractice/v1/users");
      console.log("createUsersV4");
      var newUser = {
	       "first_name" : req.body.first_name,
         "last_name" : req.body.last_name,
         "email" : req.body.email,
         "nid" : req.body.nid,
         "password" : crypt.hash(req.body.password),
      }
// Comprobar que exista ese usuarios
      var query = 'q={"nid":"' + req.body.nid +'"}&f={"_id":0, "password":0}';
      var httpClient = requestJson.createClient(mlabURLBase);
      var response = {};
      console.log("Searching user by nid="+req.body.nid);
      httpClient.get('user?'+ query + '&' + mlabAPIKey,
        function (err, resMLab, body){
          if (err) {
    	      console.log("err");
            var response = {
              "msg" : "Error buscando usuario"
            }
            res.status(500)
            res.send(response);
          } else {
              if (body.length > 0) {
    	           console.log("Usuario creado");
    	           var response = body[0];
                 res.status(409)
                 res.send(response);
              } else {
    	           console.log("User not found");
                 var query = 's={"id":-1}&l=1&f={"id":1, "_id":0}';
                 var httpClient = requestJson.createClient(mlabURLBase);
                 var response = {};
                 httpClient.get('user?'+ query + '&' + mlabAPIKey,
                   function (err, resMLab, body){
                      newUser.id = parseInt(body[0].id+1)
                      console.log("ID encontrado con exito: nuevo ID :"+newUser.id);
                      var httpClient = requestJson.createClient(mlabURLBase);
                      var response = {};
                      httpClient.post("user?"+ mlabAPIKey, newUser,
                         function (err, resMLab, body){
                           if (err) {
                             console.log("err");
                             var response = {
                               "msg" : "Error creando usuario"
                             }
                             res.status(500)
                           } else {
                              console.log("Usuario encontrado");
                              var response = {
                                 "msg" : "Usuario Creado con éxito"
                              }
                              res.status(201)
                           }
                           res.send(response);
                         }
                        );
           	       }
                  );
              }
          }

        }
        );
    }
    //####################################################################
function createUsersV5 (req,res) {
// falta probar cuando no hay nada
// falta ver que hacer con los huecos cerrados
  console.log("POST /BankPractice/v1/users");
  console.log("createUsersV5");
  var newUser = {
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "nid" : req.body.nid,
     "password" : crypt.hash(req.body.password),
  }
// Comprobar que exista ese usuarios
  var query = 'q={"nid":"' + req.body.nid +'"}&f={"_id":0, "password":0}';
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  console.log("Searching user by nid="+req.body.nid);
  httpClient.get('user?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("err");
        var response = {
          "msg" : "Error buscando usuario"
        }
        res.status(500)
        res.send(response);
      } else {
          if (body.length > 0) {
	           console.log("Usuario ya creado");
	           var response = body[0];
             res.status(409)
             res.send(response);
          } else {
	           console.log("User not found");
             var query = 's={"id":-1}&l=1&f={"id":1, "_id":0}';
             var httpClient = requestJson.createClient(mlabURLBase);
             var response = {};
             httpClient.get('user?'+ query + '&' + mlabAPIKey,
               function (err, resMLab, body){
                  if ( req.body.id === 0 ) {
                    newUser.id = 0;
                  } else {
                    newUser.id = parseInt(body[0].id+1);
                  }

                  console.log("ID encontrado con exito: nuevo ID :"+newUser.id);
                  var httpClient = requestJson.createClient(mlabURLBase);
                  var response = {};
                  httpClient.post("user?"+ mlabAPIKey, newUser,
                     function (err, resMLab, body){
                       if (err) {
                         console.log("err");
                         var response = {
                           "msg" : "Error creando usuario"
                         }
                         res.status(500)
                       } else {
                          console.log("Usuario encontrado");
                          var response = {
                             "msg" : "Usuario Creado con éxito",
                             "id" : newUser.id
                          }
                          res.status(201)
                       }
                       res.send(response);
                     }
                    );
       	       }
              );
          }
      }

    }
    );
}
//####################################################################
function checkpassword(sentPassword, userHashPassword){
  console.log("checkpassword");
  return bcrypt.compareSync (sentPassword, userHashPassword);
}
//####################################################################
function deleteUsersV2 (req,res) {
  console.log("DELETE /BankPractice/v1/users/:id");
  console.log("DeleteUsersByIdV2");
  var id = req.params.id;
  var query = 'q={"id":' + id +'}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching user by id="+id);
  var response = {};
  httpClient.get('user?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	        console.log("error obteniendo oid");
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500)
          res.send(response);
      } else {
          if (body.length > 0) {
	           console.log("Usuario a borrar :"+body[0]._id.$oid);
          	 var oid = body[0]._id.$oid;
             httpClient.delete('user/'+ oid + '?' + mlabAPIKey,
               function (err, resMLab, body){
                 if (err) {
                   console.log("error borrando usuario");
                   var response = {
                     "msg" : "Error borrando usuario"
                   }
                   res.status(500)
                 } else {
                   var response = {
                     "msg" : "Usuario Borrado !!!!"
                   }
                   res.status(200)
                 }
                 res.send(response);
               }
             );
          } else {
                console.log("body.length < 0");
                var response = {
                  "msg" : "Usuario No encontrado"
                }
                res.status(404);
                res.send(response);
          }
      }
    }
    );
}
//####################################################################
function getUserAccountV1(req, res) {
 console.log("GET /BankPractice/v2/users/:id/accounts");
 console.log("getUserAccountV1");
 var id = req.params.id;
 var query = 'q={"userId":' + id + '}';
 var httpClient = requestJson.createClient(mlabURLBase);
 httpClient.get("account?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     if (err) {
       response = {
         "msg" : "Error obteniendo cuentas."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
         res.status(200);
       } else {
         response = {
           "msg" : "El usuario no tiene cuentas."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}
//####################################################################
function getUserSpecificAccountV1(req, res) {
 console.log("GET /BankPractice/v2/users/:id/accounts/:aid");
 console.log("getUserSpecificAccountV1");
 var id = req.params.id;
 var aid = req.params.aid;
 var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}';
 var httpClient = requestJson.createClient(mlabURLBase);
 httpClient.get("account?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     if (err) {
       response = {
         "msg" : "Error obteniendo la cuenta."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body[0];
         res.status(200);
       } else {
         response = {
           "msg" : "El usuario no tiene esta cuenta."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}
//####################################################################
function createUserBankAccountV1 (req,res) {
       console.log("POST /BankPractice/v1/users/:id:/");
       console.log("createUserBankAccountV1");
       var newAcccount = {
	       "IBAN" : req.body.IBAN.replace(/ /g, ""),
         "balance" : req.body.balance,
         "userId" : parseInt(req.params.id),
         "accountId" : 0,
         "currency" : req.body.currency,
      }
       var query = 'q={"userId":'+ req.params.id +'}&s={"accountId":-1}&l=1&f={"accountId":1, "_id":0}';
       var httpClient = requestJson.createClient(mlabURLBase);
       var response = {};
       console.log("Creando cuenta para el usuario:"+req.params.id);
       httpClient.get('account?'+ query + '&' + mlabAPIKey,
         function (err, resMLab, body){
            if (body.length > 0) {
              newAcccount.accountId = parseInt(body[0].accountId+1)
              console.log("Nueva ID :"+newAcccount.accountId);
            } else {
              console.log("Usuario no encontrado o sin cuentas");
              newAcccount.accountId = 1
            }
            var httpClient = requestJson.createClient(mlabURLBase);
            var response = {};
            httpClient.post("account?"+ mlabAPIKey, newAcccount,
               function (err, resMLab, body){
                 if (err) {
                   console.log("Error creando cuenta");
                   var response = {
                     "msg" : "Error creando cuenta"
                   }
                   res.status(500)
                 } else {
                    console.log("Cuenta creada con éxito");
                    var response = {
                       "msg" : "Cuenta creada con éxito"
                    }
                    res.status(201)
                 }
                 res.send(response);
               }
            );
 	       }
        );
    };
//####################################################################
function deleteUserBankAccountV1 (req,res) {
  // Borrar cuentas y movimientos
  console.log("DELETE /BankPractice/v1/users/:id/accounts/:aid");
  console.log("deleteUserBankAccountV1");
  var id = req.params.id;
  var aid = req.params.aid;
  var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching user by userId="+id+" and accountId="+aid);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	        console.log("error obteniendo oid");
          var response = {
            "msg" : "Error borrando cuenta"
          }
          res.status(500)
          res.send(response);
      } else {
          if (body.length > 0) {
	           console.log("Usuario a borrar :"+body[0]._id.$oid);
          	 var oid = body[0]._id.$oid;
             httpClient.delete('account/'+ oid + '?' + mlabAPIKey,
               function (err, resMLab, body){
                 if (err) {
                   console.log("error borrando cuenta");
                   var response = {
                     "msg" : "Error borrando cuenta"
                   }
                   res.status(500)
                 } else {
                   var response = {
                     "msg" : "Cuenta Borrada"
                   }
                   res.status(200)
                 }
                 res.send(response);
               }
             );
          } else {
                console.log("body.length < 0");
                var response = {
                  "msg" : "Cuenta o usuario no encontrado"
                }
                res.status(404);
                res.send(response);
          }
      }
    }
    );
}
//####################################################################
function depositUserBankAccountV1 (req,res) {
  console.log("PATCH /BankPractice/v1/users/:id/accounts/:aid/deposit/:amount");
  console.log("depositUserBankAccountV1");
  var id = req.params.id;
  var aid = req.params.aid;
  var amount = req.params.amount;
  var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching user by userId="+id+" and accountId="+aid);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	        console.log("error obteniendo oid");
          var response = {
            "msg" : "Error actualizando cuenta"
          }
          res.status(500)
          res.send(response);
      } else {
          if (body.length > 0) {
	           console.log("Documento a actualizar :"+body[0]._id.$oid);
             var updateAcccount = {
      	       "IBAN" : body[0].IBAN,
               "balance" : parseFloat(body[0].balance),
               "userId" : parseInt(req.params.id),
               "accountId" : parseInt(body[0].accountId),
               "currency" : body[0].currency,
               "_id" : body[0]._id ,
             };
             var oid = 'q={"_id":"' + body[0]._id.$oid +'"}';
             console.log("Old Balance :"+updateAcccount.balance);
             updateAcccount.balance=parseFloat(updateAcccount.balance)+parseFloat(amount)
             console.log("New Balance :"+updateAcccount.balance);
             httpClient.post('account?'+ oid + '&' + mlabAPIKey, updateAcccount,
               function (err, resMLab, body){
                 if (err) {
                   console.log("error actualizando cuenta");
                   var response = {
                     "msg" : "Error actualizando cuenta"
                   }
                   res.status(500);
                 } else {
                   var response = {
                     "msg" : "Cuenta actualizada",
                     "balance": updateAcccount.balance
                   }
                   res.status(200);
                 }
                 res.send(response);
               }
             );

          } else {
                console.log("body.length < 0");
                var response = {
                  "msg" : "Cuenta o usuario no encontrado"
                }
                res.status(404);
                res.send(response);
          }
      }
    }
    );
}
//####################################################################
function paymentUserBankAccountV1 (req,res) {
  console.log("PATCH /BankPractice/v1/users/:id/accounts/:aid/payment/:amount");
  console.log("paymentUserBankAccountV1");
  var id = req.params.id;
  var aid = req.params.aid;
  var amount = req.params.amount;
  var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}';
  var httpClient = requestJson.createClient(mlabURLBase);
  console.log("Searching user by userId="+id+" and accountId="+aid);
  var response = {};
  httpClient.get('account?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	        console.log("error obteniendo oid");
          var response = {
            "msg" : "Error actualizando cuenta"
          }
          res.status(500)
          res.send(response);
      } else {
          if (body.length > 0) {
	           console.log("Documento a actualizar :"+body[0]._id.$oid);
             console.log(body[0].balance);
             if (parseFloat(body[0].balance)>0){
               var updateAcccount = {
        	       "IBAN" : body[0].IBAN,
                 "balance" : parseFloat(body[0].balance),
                 "userId" : parseInt(req.params.id),
                 "accountId" : parseInt(body[0].accountId),
                 "currency" : body[0].currency,
                 "_id" : body[0]._id ,
               };
               var oid = 'q={"_id":"' + body[0]._id.$oid +'"}';
               console.log("Old Balance :"+updateAcccount.balance);
               updateAcccount.balance=parseFloat(updateAcccount.balance)-parseFloat(amount)
               console.log("New Balance :"+updateAcccount.balance);

               httpClient.post('account?'+ oid + '&' + mlabAPIKey, updateAcccount,
                 function (err, resMLab, body){
                   if (err) {
                     console.log("error actualizando cuenta");
                     var response = {
                       "msg" : "Error actualizando cuenta"
                     }
                     res.status(500);
                   } else {
                     console.log("Cuenta actualizada");
                     var response = {
                       "msg" : "Cuenta actualizada",
                       "balance": updateAcccount.balance
                     }
                     res.status(200);
                   }
                   res.send(response);
                 }
               );
             } else {
                 console.log("La cuenta no tiene saldo.");
                 var response = {
                   "msg" : "La cuenta no tiene saldo."
                 }
                 res.status(402);
                 res.send(response);
             };
          } else {
                console.log("body.length < 0");
                var response = {
                  "msg" : "Cuenta o usuario no encontrado"
                }
                res.status(404);
                res.send(response);
          }
      }
    }
    );
}



//--------------------------------------------------------------------
module.exports.getUsersV3 = getUsersV3;
//--------------------------------------------------------------------
module.exports.createUsersV4 = createUsersV5;
//--------------------------------------------------------------------
module.exports.getUsersByIdV2 = getUsersByIdV2;
//--------------------------------------------------------------------
module.exports.deleteUsersV2 = deleteUsersV2;
//--------------------------------------------------------------------
module.exports.checkpassword = checkpassword;
//--------------------------------------------------------------------
module.exports.getUserByNidV1 = getUserByNidV1;
//--------------------------------------------------------------------
module.exports.getUserAccountV1 = getUserAccountV1;
//--------------------------------------------------------------------
module.exports.getUserSpecificAccountV1 = getUserSpecificAccountV1;
//--------------------------------------------------------------------
module.exports.createUserBankAccountV1 = createUserBankAccountV1;
//--------------------------------------------------------------------
module.exports.deleteUserBankAccountV1 = deleteUserBankAccountV1
//--------------------------------------------------------------------
module.exports.depositUserBankAccountV1 = depositUserBankAccountV1
//--------------------------------------------------------------------
module.exports.paymentUserBankAccountV1 = paymentUserBankAccountV1
//--------------------------------------------------------------------
