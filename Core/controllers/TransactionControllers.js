require('dotenv').config()
const dateTime = require('node-datetime');
const userController = require('./UserControllers')
//const io = require ("../io");
//const crypt = require("../crypt")
//const bcrypt = require("bcrypt")
const requestJson = require('request-json');
const mlabURLBase='https://api.mlab.com/api/1/databases/apitechu8mac/collections/';
const mlabAPIKey='apiKey=' + process.env.MLAB_API_KEY
//####################################################################
function transactionDoneV1 (req,res) {
  console.log("POST /BankPractice/v1/transaction");
  var dt = dateTime.create();
  var newTransaction = {
    "accountId" : req.body.accountId,
    "balance" : parseFloat(req.body.balance),
    "userId" : parseInt(req.body.userId),
    "accountId" : parseInt(req.body.accountId),
    "type" : req.body.type,
    "amount" : parseFloat(req.body.amount),
    "who" : req.body.who,
    "date" : dt.format('Y/m/d H:M:S'),
 }
  console.log (newTransaction);
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  console.log("Creando el movimiento");
  httpClient.post("transaction?"+ mlabAPIKey, newTransaction,
  function (err, resMLab, body){
           if (err) {
             console.log("Error accediendo a MLAB para registrar un movimiento");
             var response = {
               "msg" : "Error accediendo a MLAB para registrar un movimiento"
             }
             res.status(500)
           } else {
              console.log("Movimiento creado con éxito");
              var response = {
                 "msg" : "Movimiento creado con éxito"
              }
              res.status(201)
           }
           res.send(response);
         }
 );
}
//####################################################################
function getTransactionByIdV1 (req,res) {
  console.log("GET /BankPractice/v1/transaction/:id/:aid");
  console.log("getTransactionByIdV1");
  var id = req.params.id;
  var aid = req.params.aid;
  var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}&f={"_id":0}';
  console.log("Searching user by id="+id+" aid="+aid);
  var httpClient = requestJson.createClient(mlabURLBase);
  var response = {};
  httpClient.get('transaction?'+ query + '&' + mlabAPIKey,
    function (err, resMLab, body){
      if (err) {
	      console.log("Error obteniendo informacion");
        var response = {
          "msg" : "Error obteniendo informacion"
        }
        res.status(500)
      } else {
          if (body.length > 0) {
	           console.log("transacciones encontradas");
	           var response = body;
             res.status(200)
          } else {
	           console.log("body.length < 0");
             var response = {
                "msg" : "Cuenta o usuario no encontrado."
             }
            res.status(404);
          }
      }
      res.send(response);
    }
    );
}
//####################################################################
// function transactionDepositV1 (req,res) {
//   console.log("POST /BankPractice/v1/transaction/deposit/:id/:aid/:amount");
//   console.log("POST /BankPractice/v1/transactionDepositV1");
//   var id = req.params.id;
//   var aid = req.params.aid;
//   var amount = req.params.amount;
//   var query = 'q={"$and":[{"userId":' + id + '},{"accountId":' + aid + '}]}&f={"balance":1}';
//   var httpClient = requestJson.createClient(mlabURLBase);
//   console.log("Searching user by userId="+id+" and accountId="+aid);
//   var response = {};
//   httpClient.get('account?'+ query + '&' + mlabAPIKey,
//     function (err, resMLab, body){
//       if (err) {
//           console.log("error obteniendo oid");
//           var response = {
//             "msg" : "Error buscando cuenta en MLAB"
//           }
//           res.status(500)
//           res.send(response);
//       } else {
//           if (body.length > 0) {
//             if (body[0].balance == null){
//               console.log("null pongo a 0 la cuenta");
//               body[0].balance = 0;
//             };
//             console.log("Balance :"+body[0].balance);
//              if (parseFloat(body[0].balance)>0){
//                  console.log("Hay dinero");
//                  const promise = new Promise((resolve, reject) => {
//                     userController.depositUserBankAccountV1 (req,res);
//                     console.log("-_()_-")
//                  });
//                  promise.then( function (req) {
//                        // meter los datos de cuenta y eso
//                        req.body.userId = req.params.id;
//                        req.body.accountId = req.params.aid;
//                        //req.body.balance
//                        console.log("OEOEOEOEO");
//                        transactionDoneV1 (req,res);
//                      },
//                      function (error) {
//                        console.log("ERROR PROMISE");
//                      }
//                    );
//              } else{
//                console.log("NO Hay dinero");
//              }
//
//
//           } else {
//                 console.log("body.length < 0");
//                 var response = {
//                   "msg" : "Cuenta o usuario no encontrado"
//                 }
//                 res.status(404);
//                 res.send(response);
//           }
//       }
//     }
//     );
//
// }
//

//--------------------------------------------------------------------
module.exports.transactionDoneV1 = transactionDoneV1;
//--------------------------------------------------------------------
module.exports.getTransactionByIdV1 = getTransactionByIdV1;
//--------------------------------------------------------------------
//module.exports.transactionPaymentV1 = transactionPaymentV1;
//--------------------------------------------------------------------
//module.exports.transactionDepositV1 = transactionDepositV1;
//--------------------------------------------------------------------
