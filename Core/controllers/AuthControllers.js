//const io = require('../io');
const crypt = require("../crypt")
const userController = require('./UserControllers')
const requestJson = require('request-json');
const mlabBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8mac/collections/';
require('dotenv').config();
const mlabAPIKey='apiKey=' + process.env.MLAB_API_KEY

//####################################################################
function loginAdminV1(req,res) {
   console.log("POST /apitechu/v1/loginadmin");
   console.log(req.body);
   var datos_entrada = req.body;
   console.log(datos_entrada.email);
   console.log(datos_entrada.password);
   var users = require("../usuarios.json");

   var indexofemail = users.findIndex (
         function (element) {
             return element.email == datos_entrada.email;
           }
   );
   console.log(indexofemail);
   if (indexofemail == -1) {
     res.status(404)
     response = {
       "msg" : "Usuario No encontrado"
     }
   } else {
   console.log (users[indexofemail].password)
   if (users[indexofemail].password == datos_entrada.password) {
     res.status(200)
     response = {
       "msg" : "Login correcto", "idUsuario" : "-1"
     }
     res.send (response);
   } else {
       res.status(401)
       response = {
         "msg" : "Login incorrecto"
       }
     }
   }
   res.send (response);
}
//####################################################################
function loginUserV2(req,res) {
  console.log("POST /BankPractice/v1/login");
  console.log("loginUserV2");
  var datos_entrada = req.body;
  var query = 'q={"email":"' + datos_entrada.email + '"}';
  var httpClient = requestJson.createClient(mlabBaseURL);
  httpClient.get(mlabBaseURL+"user?" + query + '&'+ mlabAPIKey,
      function(err,resMLab,body){
        if (err) {
           console.log("Error obteniendo usuario");
           var response = {
             "msg" : "Error obteniendo usuario"
           }
           res.status(500)
        } else {
           if (body.length > 0) {
              var user_devuelto = body[0];
              if (userController.checkpassword(datos_entrada.password,user_devuelto.password)){
                response = {
                  "msg" : "Login correcto", "idUsuario" : user_devuelto.id
                }
                var putBody = '{"$set":{"logged":true}}';
                httpClient.put("user?" + query + '&'+ mlabAPIKey, JSON.parse(putBody),
                  function(err,resMLab,body){
                    if (err) {
                      console.log("Error actualizando");
                      //body : {"msg":"Error actualizando estado"};
                    } else {
                      console.log("Estado actualizado en MLAB");
                    }
                  })
                  res.status(200);
              } else {
                console.log("Login incorrecto");
                response = {
                  "msg" : "Login incorrecto"
                }
                res.status(401);
              }
           } else {
             console.log("Usuario No encontrado");
             var response = {
               "msg" : "Usuario No encontrado"
             }
             res.status(404);
           }
        }
        res.send(response);
      }
  );
}
//####################################################################
function logoutUserV2(req,res) {
    console.log("POST /BankPractice/v1/logout/");
    console.log("logoutUserV2");
    var datos_entrada = req.body;
    var query = 'q={"id":' + datos_entrada.id +'}';
    var httpClient = requestJson.createClient(mlabBaseURL);
      httpClient.get("user?" + query + '&'+ mlabAPIKey,
        function(err,resMLab,body){
          if (err) {
            console.log("Error obteniendo usuario");
            var response = {
               "msg" : "Error obteniendo usuario"
             }
            res.status(500)
          } else {
             if (body.length > 0) {
		             var user_devuelto = body[0];
		             if (user_devuelto.logged){
              		  console.log("Logout correcto");
                    response = {
                      "msg" : "Logout correcto", "idUsuario" : user_devuelto.id
                    }
              		  var putBody = '{"$unset":{"logged":""}}';
                    httpClient.put("user?" + query + '&'+ mlabAPIKey, JSON.parse(putBody),
                      function(err,resMLab,body){
                          if(err){
                            console.log("Error actualizando");
                            //body : {"msg":"Error actualizando estado"};
                          } else {
                            console.log("Logout correcto");
                            res.status(200);
                          }
                      })
  		           } else {
                      console.log("Logout incorrecto");
              		    var response = {
                                    "msg" : "Logout incorrecto"
                                   }
                      res.status(409);
  		           }
	           } else {
                 var response = {
                   "msg" : "Usuario No encontrado"
                 }
                 res.status(404);
             }
          }
          res.send(response);
        }
      );
}
//####################################################################
module.exports.loginUserV2 = loginUserV2;
//--------------------------------------------------------------------
module.exports.logoutUserV2 = logoutUserV2;
//--------------------------------------------------------------------
module.exports.loginAdminV1 = loginAdminV1;
//--------------------------------------------------------------------
