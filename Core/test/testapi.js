require('dotenv').config()
const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');
const port = process.env.PORT ;
const testURLBase='http://localhost:'+port+'/BankPractice/v1';
console.log(testURLBase);

chai.use(chaihttp);

var should = chai.should();
var server = require('../server');

describe('Test de API de Usuarios [CREACION/CONSULTAS/ERRORES/LOGIN/LOGOUT/TRANSACCIONES]',
      function(){
        it('POST 404 /BankPractice/v1/login', function(done){
          chai.request(testURLBase)
            .post('/login')
            .send(
              {
                "email": "spider@man",
                "password": "fail"
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Usuario No encontrado");
                done();
              }
            )
            }
        ),
        it('POST 401 /BankPractice/v1/login', function(done){
          chai.request(testURLBase)
            .post('/login')
            .send(
              {
                "email": "spider@man.net",
                "password": "fail"
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(401);
                res.body.msg.should.have.be.eql("Login incorrecto");
                done();
              }
            )
            }
        ),
        it('POST 200 /BankPractice/v1/login', function(done){
          chai.request(testURLBase)
            .post('/login')
            .send(
              {
                "email": "spider@man.net",
                "password": "gr34tp0w3r"
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Login correcto");
                res.body.should.have.property("idUsuario");
                done();
              }
            )
            }
        ),
        it('POST 404 /BankPractice/v1/logout', function(done){
          chai.request(testURLBase)
            .post('/logout')
            .send(
              {
                "id": 100
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Usuario No encontrado");
                done();
              }
            )
            }
        ),
        it('POST 409 /BankPractice/v1/logout', function(done){
          chai.request(testURLBase)
            .post('/logout')
            .send(
              {
                "id": 2
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(409);
                res.body.msg.should.have.be.eql("Logout incorrecto");
                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/users', function(done){
          chai.request(testURLBase)
            .get('/users')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                //res.body.msg.should.have.be.eql("Hola desde API TechU")
                for (user of res.body){
                  user.should.have.property("email");
                  user.should.have.property("id");
                  user.should.have.property("first_name");
                  user.should.have.property("last_name");
                  user.should.have.property("password");
                }
                done();
              }
            )
            }
        ),
        it('POST 201 /BankPractice/v1/users', function(done){
          chai.request(testURLBase)
            .post('/users')
            .send(
              {
                "first_name" : "Clark",
                "last_name" : "Kent",
                "email" : "superm@n.net",
                "password" : "sup3r",
                "nid" : "12345678",
                "id" : 0
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(201);
                res.body.should.have.property("msg");
                res.body.should.have.property("id");
                done();
              }
            )
            }
        ),
        it('POST 409 /BankPractice/v1/users', function(done){
          chai.request(testURLBase)
            .post('/users')
            .send(
              {
                "first_name" : "Clark",
                "last_name" : "Kent",
                "email" : "superm@n.net",
                "password" : "sup3r",
                "nid" : "12345678",
                "id" : 0
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(409);
                res.body.should.have.property("first_name");
                res.body.should.have.property("last_name");
                res.body.should.have.property("email");
                res.body.should.have.property("nid");
                res.body.should.have.property("id");
                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/users/:id', function(done){
          chai.request(testURLBase)
            .get('/users/0')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                //res.body.msg.should.have.be.eql("Hola desde API TechU")
                res.body.should.have.property("email");
                res.body.should.have.property("id");
                res.body.should.have.property("first_name");
                res.body.should.have.property("last_name");
                res.body.should.have.property("password");

                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/users/nid/:nid', function(done){
          chai.request(testURLBase)
            .get('/users/nid/12345678')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                //res.body.msg.should.have.be.eql("Hola desde API TechU")
                res.body.should.have.property("email");
                res.body.should.have.property("id");
                res.body.should.have.property("first_name");
                res.body.should.have.property("last_name");
                res.body.should.have.property("password");

                done();
              }
            )
            }
        ),
        it('POST 201 1 /BankPractice/v1/users/:id/accounts', function(done){
          chai.request(testURLBase)
            .post('/users/0/accounts')
            .send(
              {
                  "IBAN": "ES00 0123 4567 8900 TEST TEST MAC",
                  "balance": "1000,10",
                  "userId": 0,
                  "currency": "euro"
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(201);
                res.body.msg.should.have.be.eql("Cuenta creada con éxito")
                done();
              }
            )
            }
        ),
        it('POST 201 2 /BankPractice/v1/users/:id/accounts', function(done){
          chai.request(testURLBase)
            .post('/users/0/accounts')
            .send(
              {
                  "IBAN": "ES00 0000 0000 0000 TEST TEST MAC",
                  "balance": "1000,10",
                  "userId": 0,
                  "currency": "euro"
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(201);
                res.body.msg.should.have.be.eql("Cuenta creada con éxito")
                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/users/:id/accounts', function(done){
          chai.request(testURLBase)
            .get('/users/0/accounts')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                for (user of res.body){
                  user.should.have.property("IBAN");
                  user.should.have.property("balance");
                  user.should.have.property("userId");
                  user.should.have.property("accountId");
                  user.should.have.property("currency");
                }

                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/users/:id/accounts/:aid', function(done){
          chai.request(testURLBase)
            .get('/users/0/accounts/1')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.should.have.property("IBAN");
                res.body.should.have.property("balance");
                res.body.should.have.property("userId");
                res.body.should.have.property("accountId");
                res.body.should.have.property("currency");

                done();
              }
            )
            }
        ),
        it('PATCH 200 /BankPractice/v1/users/:id/accounts/:aid/deposit/:amoun', function(done){
          chai.request(testURLBase)
            .patch('/users/0/accounts/1/deposit/100')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Cuenta actualizada")
                done();
              }
            )
            }
        ),
        it('PATCH 404 /BankPractice/v1/users/:id/accounts/:aid/deposit/:amoun', function(done){
          chai.request(testURLBase)
            .patch('/users/0/accounts/100/deposit/100')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Cuenta o usuario no encontrado")
                done();
              }
            )
            }
        ),

        it('PATCH 404 /BankPractice/v1/users/:id/accounts/:aid/payment/:amount', function(done){
          chai.request(testURLBase)
            .patch('/users/0/accounts/100/payment/100')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Cuenta o usuario no encontrado")
                done();
              }
            )
            }
        ),
        it('PATCH 200 /BankPractice/v1/users/:id/accounts/:aid/payment/:amount', function(done){
          chai.request(testURLBase)
            .patch('/users/0/accounts/1/payment/10000')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Cuenta actualizada")
                done();
              }
            )
            }
        ),
        it('PATCH 402 /BankPractice/v1/users/:id/accounts/:aid/payment/:amount', function(done){
          chai.request(testURLBase)
            .patch('/users/0/accounts/1/payment/10000')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(402);
                res.body.msg.should.have.be.eql("La cuenta no tiene saldo.")
                done();
              }
            )
            }
        ),

        it('GET 404 /BankPractice/v1/users:id', function(done){
          chai.request(testURLBase)
            .get('/users/a')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Usuarios No encontrado")
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/users/nid/:nid', function(done){
          chai.request(testURLBase)
            .get('/users/nid/nid')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Usuarios No encontrado")
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/users/:id/accounts', function(done){
          chai.request(testURLBase)
            .get('/users/a/accounts')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("El usuario no tiene cuentas.")
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/users/:id/accounts/:aid', function(done){
          chai.request(testURLBase)
            .get('/users/1/accounts/a')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("El usuario no tiene esta cuenta.")
                done();
              }
            )
            }
        )
        it('POST 200 /BankPractice/v1/logout', function(done){
          chai.request(testURLBase)
            .post('/logout')
            .send(
              {
                "id": 10
              }
            )
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Logout correcto");
                res.body.should.have.property("idUsuario");
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/transaction/0/0', function(done){
          chai.request(testURLBase)
            .get('/transaction/0/0')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Cuenta o usuario no encontrado.");
                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/transaction/:id/:aid', function(done){
          chai.request(testURLBase)
            .get('/transaction/10/8')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                for (user of res.body){
                  user.should.have.property("accountId");
                  user.should.have.property("balance");
                  user.should.have.property("userId");
                  user.should.have.property("type");
                  user.should.have.property("amount");
                  user.should.have.property("who");
                  user.should.have.property("date");
                }

                done();
              }
            )
            }
        )
      }
    )
describe('Test de API de Cuentas [CREACION/CONSULTAS/ERRORES]',
      function(){
        it('GET 200 /BankPractice/v1/account', function(done){
          chai.request(testURLBase)
            .get('/account')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                //res.body.msg.should.have.be.eql("Hola desde API TechU")
                for (account of res.body){
                  account.should.have.property("IBAN");
                  account.should.have.property("balance");
                  account.should.have.property("userId");
                  account.should.have.property("accountId");
                  account.should.have.property("currency");
                }
                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/account/user/:userId', function(done){
          chai.request(testURLBase)
            .get('/account/user/1')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                //res.body.msg.should.have.be.eql("Hola desde API TechU")
                for (account of res.body){
                  account.should.have.property("IBAN");
                  account.should.have.property("balance");
                  account.should.have.property("userId");
                  account.should.have.property("accountId");
                  account.should.have.property("currency");
                }

                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/account/:aid', function(done){
          chai.request(testURLBase)
            .get('/account/3')
            .end(
              function(err, res){
                console.log('Request finished');
                //console.log(err);
                res.should.have.status(200);
                res.body.should.have.property("IBAN");
                res.body.should.have.property("balance");
                res.body.should.have.property("userId");
                res.body.should.have.property("accountId");
                res.body.should.have.property("currency");

                done();
              }
            )
            }
        ),
        it('GET 200 /BankPractice/v1/account/IBAN/:iban', function(done){
          chai.request(testURLBase)
            .get('/account/IBAN/ES7214910001282018124012')
            .end(
              function(err, res){
                console.log('Request finished');
                //console.log(err);
                res.should.have.status(200);
                res.body.should.have.property("IBAN");
                res.body.should.have.property("balance");
                res.body.should.have.property("userId");
                res.body.should.have.property("accountId");
                res.body.should.have.property("currency");
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/account/:aid', function(done){
          chai.request(testURLBase)
            .get('/account/user/a')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Cuenta No encontrada")
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/account/:userId', function(done){
          chai.request(testURLBase)
            .get('/account/a')
            .end(
              function(err, res){
                console.log('Request finished');
                res.body.msg.should.have.be.eql("Cuenta No encontrada")
                done();
              }
            )
            }
        ),
        it('GET 404 /BankPractice/v1/account/IBAN/:iban', function(done){
          chai.request(testURLBase)
            .get('/account/IBAN/ESTENOESTA')
            .end(
              function(err, res){
                console.log('Request finished');
                //console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("IBAN No encontrado")
                done();
              }
            )
            }
        )
      }
    )
describe('Test de API de Usuarios [BORRADO USUARIOS/CUENTAS]',
      function(){
        it('DELETE 200 1 /BankPractice/v1/users/:id/accounts/:aid', function(done){
          chai.request(testURLBase)
            .delete('/users/0/accounts/1')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Cuenta Borrada")
                done();
              }
            )
            }
        ),
        it('DELETE 200 2 /BankPractice/v1/users/:id/accounts/:aid', function(done){
          chai.request(testURLBase)
            .delete('/users/0/accounts/2')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Cuenta Borrada")
                done();
              }
            )
            }
        ),
        it('DELETE 404 /BankPractice/v1/users/:id/accounts/:aid', function(done){
          chai.request(testURLBase)
            .get('/users/0/accounts/0')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("El usuario no tiene esta cuenta.")
                done();
              }
            )
            }
        ),
        it('DELETE 200 /BankPractice/v1/users/:id', function(done){
          chai.request(testURLBase)
            .delete('/users/0')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(200);
                res.body.msg.should.have.be.eql("Usuario Borrado !!!!")
                done();
              }
            )
            }
        ),
        it('DELETE 404 /BankPractice/v1/users/:id', function(done){
          chai.request(testURLBase)
            .delete('/users/0')
            .end(
              function(err, res){
                console.log('Request finished');
                console.log(err);
                res.should.have.status(404);
                res.body.msg.should.have.be.eql("Usuario No encontrado")
                done();
              }
            )
            }
        )

      }
    )
