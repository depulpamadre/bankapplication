# BankPractice API documentation version v1

Probados todos con OK. Salvo los PATCH convertidos en GET.


## /BankPractice/v1/loginadmin
Realiza la autenticación de un usuario administrador

### /BankPractice/v1/loginadmin

* **post**: Realiza la autenticación de un usuario

## /BankPractice/v1/login
Realiza la autenticación de un usuario

### /BankPractice/v1/login

* **post**: Realiza la autenticación de un usuario

## /BankPractice/v1/logout
Realiza la desconexion de un usuario

### /BankPractice/v1/logout/{id}

* **post**: Realiza la autenticación de un usuario

## /BankPractice/v1/users
Acciones asociadas a las actividades desarrolladas por el usuario y las cuentas

### /BankPractice/v1/users

* **post**: Crea usuarios
* **get**: Obtiene TODOS los usuarios de MLAB

### /BankPractice/v1/users/{userId}
Operaciones básicas de usuario

* **get**: Obtiene un usuario de MLAB
* **delete**: Borra un usuario

### /BankPractice/v1/users/{userId}/account
Operaciones sobre las cuentas de {userId}

* **get**: Obtiene TODAS las cuentas de un usuario en MLAB
* **post**: Crea una cuenta para {userId} con saldo dado

### /BankPractice/v1/users/{userId}/account/{idAccount}
Operaciones sobre as cuentas de {userId} {idAccount}

* **delete**: Borra una cuenta del usuario userId
* **get**: Obtiene la información de la cuenta idAccount del usuario userId

### /BankPractice/v1/users/{userId}/account/{idAccount}/deposit/{amount}
Realiza un incremento en el valor del saldo existente

* **patch**: 
* **get**: 

### /BankPractice/v1/users/{userId}/account/{idAccount}/payment/{amount}

* **patch**: Realiza un decremento en el valor del saldo existente
* **get**: Realiza un decremento en el valor del saldo existente

### /BankPractice/v1/users/nid/{nid}
Búsqueda por NID

* **get**: Obtiene un usuario de MLAB por nid

## /BankPractice/v1/account
Operaciones sobre cuentas directamente

### /BankPractice/v1/account

* **get**: Obtiene la información de todas las cuentas

### /BankPractice/v1/account/{accountId}
Obtención de la cuenta {accountId}

* **get**: Obtiene la información de una cuenta

### /BankPractice/v1/account/user/{userId}
Obtención de las cuentas de {userId}

* **get**: Obtiene la información de las cuentas de un usuario

### /BankPractice/v1/account/IBAN/{IBAN}
Obtención de una cuenta por el IBAN

* **get**: Obtiene la información de una cuentas de un usuario por el IBAN

## /BankPractice/v1/transaction
Creación del movimiento

### /BankPractice/v1/transaction

* **post**: Registra la transacción realizada

### /BankPractice/v1/transaction/{idUser}/{idAccount}
Consulta sobre las cuentas de {userId} {idAccount}

* **get**: Obtiene los movimientos de la cuenta idAccount del usuario userId

