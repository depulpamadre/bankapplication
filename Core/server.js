require('dotenv').config()
const express = require('express')
const raml2html = require('raml2html')
const app = express();
//const fs=require('fs');
const port = process.env.PORT ;
// const io = require ("./io");
const userController = require ("./controllers/UserControllers");
const authController = require ("./controllers/AuthControllers");
const accountController = require ("./controllers/AccountControllers");
const transactionController = require ("./controllers/TransactionControllers");
app.use(express.json());
app.listen(port);
var enableCORS = function (req, res, next){
  res.set("Access-Control-Allow-Origin","*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");
  next();
}
app.use(enableCORS);

function handleAbout(req, res) {
  console.log('> GET handleAbout...');
  const slateConfig = raml2html.getConfigForTheme();

  raml2html.render('./raml/api.raml', slateConfig)
    .then((html) => {
      res.send(html);
    })
    .catch((error) => {
      console.log(`< error: ${error}`);
      res.status(500).send({ error: 'RAML not allowed' });
    });
}
//####################################################################
app.post("/BankPractice/v1/login",authController.loginUserV2);
app.post("/BankPractice/v1/logout/",authController.logoutUserV2);
app.post("/BankPractice/v1/loginadmin/",authController.loginAdminV1);
//--------------------------------------------------------------------
app.get("/BankPractice/v1/users", userController.getUsersV3);
app.get("/BankPractice/v1/users/:id",userController.getUsersByIdV2);
app.get("/BankPractice/v1/users/nid/:nid",userController.getUserByNidV1);
app.get("/BankPractice/v1/users/:id/accounts",userController.getUserAccountV1);
app.get("/BankPractice/v1/users/:id/accounts/:aid",userController.getUserSpecificAccountV1);
app.post("/BankPractice/v1/users",userController.createUsersV4);
app.post("/BankPractice/v1/users/:id/accounts",userController.createUserBankAccountV1);
app.delete("/BankPractice/v1/users/:id",userController.deleteUsersV2);
app.delete("/BankPractice/v1/users/:id/accounts/:aid",userController.deleteUserBankAccountV1);
app.patch("/BankPractice/v1/users/:id/accounts/:aid/deposit/:amount",userController.depositUserBankAccountV1);
app.get("/BankPractice/v1/users/:id/accounts/:aid/deposit/:amount",userController.depositUserBankAccountV1);
app.patch("/BankPractice/v1/users/:id/accounts/:aid/payment/:amount",userController.paymentUserBankAccountV1);
app.get("/BankPractice/v1/users/:id/accounts/:aid/payment/:amount",userController.paymentUserBankAccountV1);
//--------------------------------------------------------------------
app.get("/BankPractice/v1/rabout",handleAbout);
//--------------------------------------------------------------------
app.get("/BankPractice/v1/account", accountController.getAccountsV1);
app.get("/BankPractice/v1/account/user/:userId",accountController.getAccountsByUserIdV1);
app.get("/BankPractice/v1/account/:aid", accountController.getAccountsByIdV1);
app.get("/BankPractice/v1/account/IBAN/:iban", accountController.getAccountsByIBANV1);
//--------------------------------------------------------------------
app.post("/BankPractice/v1/transaction", transactionController.transactionDoneV1);
app.get("/BankPractice/v1/transaction/:id/:aid", transactionController.getTransactionByIdV1);
//app.post("/BankPractice/v1/transaction/deposit/:id/:aid/:amount", transactionController.transactionDepositV1);
//--------------------------------------------------------------------
